---
label: Mythic Table Docs
icon: book
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

![Mythic Table](./images/mythic_logo.png)

[![Netlify Status](https://api.netlify.com/api/v1/badges/72a59d18-6ccb-4474-ba9e-f2a0610771e5/deploy-status)](https://app.netlify.com/sites/mythic-table-docs/deploys)

Welcome to the documentation pages for Mythic Table! This documentation is split into several parts.

## Design documentation
This is provided to help new and current developers understand how Mythic Table is designed. It's there to ensure that we have a clear understanding of the features we're providing so that we can iterate on this feature set and come up with new features or releases. It's also there to ensure to prevent regressions and keep the User Interface and Server design consistent.

## Manuals
1. The **Player's Manual** which shows how to use Mythic Table if you're a player.
2. The **Game Master's Manual** which shows how to use Mythic Table if you're a game master.
3. The **Server Administrator's Manual** which shows how to set up Mythic Table on your server or system if you want to run Mythic Table yourself.

Documentation is provided per-version of Mythic Table so that older versions which may have retired or different features can be kept in place, so you'll never be stuck if you're using an old version.
