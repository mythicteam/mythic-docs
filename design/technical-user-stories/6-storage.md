---
label: Storage
icon: database
order: 0
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
* As a Server Administrator, I must be able to configure different options for storage, so I can use Mythic Table how I see fit within my architecture
    * As an Application, I must provide an in-memory option, so that Server Administrators can trial the use of the application without committing to architecture
    * As an Application, I must provide a MongoDB option, so that Server Administrators can use a persistent MongoDB instance to store data for Mythic Table
* As an Application, I must support the use of Redis as an optional storage medium, so that socket data can be stored and re-played to connected Users