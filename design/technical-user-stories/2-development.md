---
label: Development and debug modes
icon: gear
order: 5
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a Developer, I require a debug modes, so that I can more effectively debug the application and develop software