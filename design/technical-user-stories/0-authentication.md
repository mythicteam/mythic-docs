---
label: Authentication
icon: person
order: 7
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As an Application, I must support integration with a third-party authentication provider shared with the client, so I can validate the authentication of sessions using the software
