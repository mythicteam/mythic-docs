---
label: Image storage
icon: file-media
order: 4
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As an Application, I must support the configurability of Google Image Storage, so that images can be stored in a Third Party storage solution.
* As an Application, I must support configuratibility of Google Image Storage using a credential file, so that I can authenticate with Google services.