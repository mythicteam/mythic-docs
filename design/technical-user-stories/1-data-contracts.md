---
label: Data contracts
icon: repo
order: 6
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
* As an Application, I require a OpenAPI Specification, so that I can explain the contract I provide.