---
label: Technical User Stories
icon: cpu
order: 1
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

[!ref text="0 - Authentication"](0-authentication.md)

These user stories outline basic requirements we have for authentication.

[!ref text="1 - Data contracts"](1-data-contracts.md)

These user stories outline basic requirements we have for how the server and client communicate and how that is documented.

[!ref text="2 - Development & Debug modes"](2-development.md)

These user stories outline basic requirements for mode within our project so that developers can debug and develop things more easily.

[!ref text="3 - Environments"](3-environments.md)

These user stories outline basic requirements for how we treat different environments, such as development and production environments.

[!ref text="4 - Image storage"](4-image-storage.md)

These user stories outline basic requirements for third-party image storage.

[!ref text="5 - Security"](5-security.md)

These user stories outline basic requirements for secure operation of the software.

[!ref text="6 - Data Storage"](6-storage.md)

These user stories outline basic requirements for data storage.

