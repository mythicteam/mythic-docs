---
label: Environments
icon: server
order: 4
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a System, I require a production mode, so that developer-specific and security-critical information can be obscured from the public
