---
label: Security
icon: lock
order: 3
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As an Application deployed in production context, I must restrict the origin of clients connecting to me, so that I can reduce MITM and XSS attacks on insecure hosts.