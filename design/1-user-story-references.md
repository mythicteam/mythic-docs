---
label: Epic & User Story Project References
icon: telescope
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

This page lists out user story references which are used in source control. These are prefixed in a formal way so that they can be specifically referenced in tasks and bugs, so that then we are absolutely clear what a bug contributes towards. This document also outlines when a user story and epic was introduced so we can track the progression of features.

## Conventions
* Epics are high-level goals the software provides
* User Stories outline high-level requirements for the software
* Epics are prefixed with *MT-EPIC-*
* User Stories are prefixed with *MT-STORY-*

## Epics
| Reference | Description |
|---|---|
| MT-EPIC-1 | Allow a User To Log Into The Application |
| MT-EPIC-2 | Allow a user to play Virtual Tabletop (VTT) Games |
| MT-EPIC-3 | Provide a damn-fine technological solution |
| MT-EPIC-4 | Login with a Third-Party Authentication Provider |
| MT-EPIC-5 | Application First Run |
| MT-EPIC-6 | Campaigns – Finding and joining campaigns/games |
| MT-EPIC-7 | Campaigns – The Tutorial Campaign |
| MT-EPIC-8 | Campaigns – Creating campaigns/games |
| MT-EPIC-9 | Campaigns – Editing campaigns/games |
| MT-EPIC-10 | Campaigns – Switching/Selecting campaigns/games |
| MT-EPIC-11 | Campaigns – Game Master User Controls |
| MT-EPIC-12 | User Interface – Grid |
| MT-EPIC-13 | User Interface – Drawings |
| MT-EPIC-14 | User Interface – Fog of War |
| MT-EPIC-15 | User Interface – Game Board |
| MT-EPIC-16 | User Interface – Maps and Map Images |
| MT-EPIC-17 | User Interface – Map Edit Mode |
| MT-EPIC-18 | User Interface – Map View Mode |
| MT-EPIC-19 | User Interface – Move-User-To-Map Mode |
| MT-EPIC-20 | User Interface – Account View Mode |
| MT-EPIC-21 | User Interface – Chat |
| MT-EPIC-22 | User Interface – Dice Rolls |
| MT-EPIC-23 | User Interface – Quick Dice Rolls |
| MT-EPIC-24 | User Interface – Character Library |
| MT-EPIC-25 | User Interface – Tokens |
| MT-EPIC-26 | Technical – Authentication |
| MT-EPIC-27 | Technical – Data Contracts |
| MT-EPIC-28 | Technical – Development Best Practices |
| MT-EPIC-29 | Technical – Environments |
| MT-EPIC-30 | Technical – Image Storage |
| MT-EPIC-31 | Technical – Security considerations (back-end) |
| MT-EPIC-32 | Technical – Storage |

## User Stories
| Epic | Reference | Description |
|---|---|---|
| MT-EPIC-1 | MT-STORY-1 | As a Visitor, I must be able to access the login page of the application, so that I can authenticate |
| MT-EPIC-4 | MT-STORY-2 | As an Application, I require user authentication which requires users to authenticate to use the application via username and password so  that Users can control access to their games. |
| MT-EPIC-4 | MT-STORY-3 | As an Application, I require the process of authentication (checking, storage, and validation of user credentials) to be handled by an external provider, to reduce technical complexity. |
| MT-EPIC-4 | MT-STORY-4 | As an Application, I must abide by the registration policies of the authentication as set by the external provider, so that the policy set by the Server Administrator is applied correctly. |
| MT-EPIC-4 | MT-STORY-5 | As an Application, I must allow "Guest Users" to use the interface without need for a username and/or password, so that users may use me  without the need to register via the external provider. |
| MT-EPIC-4 | MT-STORY-6 | As a guest User, I require a randomly-allocated, unique, and human-readable name, so that other players can identify me uniquely. |
| MT-EPIC-4 | MT-STORY-7 | As a Visitor, if the policies of the external provider allow it, I must be able to register for a new account. |
| MT-EPIC-4 | MT-STORY-8 | As a User, I must be required to re-authenticate if my token expires, so that the possibility of my account or credentials being used  without my knowledge is minimised. |
| MT-EPIC-4 | MT-STORY-9 | As an Application, I must not display the user interface to users who have failed to authenticate according to the policies set by the external provider, so that the policy set by the Server Administrator is applied correctly. |
| MT-EPIC-4 | MT-STORY-10 | As an Application, I require the use of token-based authentication when a User is using the application, so that Users can control access to their games. |
| MT-EPIC-4 | MT-STORY-11 | As a User, I must be able to request a "forgotten password" via the interface, so that if I forget my password, I can follow the process  set by the external provider and regain access to my account |
| MT-EPIC-4 | MT-STORY-12 | As a User, I must be able to request "remember me" so that if I close my browser and return to the Application, I will retain my session provided that it is not expired, so that I do not need to repeatedly log into the application. |
| MT-EPIC-2 | MT-STORY-13 | As a User, I must be authenticated in order to be able to view the Main Application Flow, so that each user of the software has an  identity within the Application |
| MT-EPIC-5 | MT-STORY-14 | As a User, I should be shown a display showing features available in the Application, so that I can review ways I can use the  application, be aware of the state of development, and view social media links for the Mythic Table project. 
| MT-EPIC-6 | MT-STORY-15 | As a User, I must be able to view a list of games that are available to me, so that I can join games I want to play. |
| MT-EPIC-7 | MT-STORY-16 | As an Application, I should provide a tutorial campaign to Users, so that they can familiarise themselves with gameplay in the  Application. |
| MT-EPIC-7 | MT-STORY-17 | As a User, I should be able to practice clicking and dragging an character token, so that I can learn how to move characters around the  Game Board. |
| MT-EPIC-7 | MT-STORY-18 | As a User, I should be able to practice panning and zooming the Game Board, so that I can learn how to view the game environment. |
| MT-EPIC-7 | MT-STORY-19 | As a User, I should be able to practice switching between multiple maps within a campaign, so that I can learn how to navigate maps  within my campaign.|
| MT-EPIC-7 | MT-STORY-20 | As a User, I should be able to practice adding new maps within a campaign, so that I can learn how to expand my campaigns. |
| MT-EPIC-7 | MT-STORY-21 | As a User, I should be able to practice deleting character tokens within a campaign, so I can learn how to clean up campaigns. |
| MT-EPIC-7 | MT-STORY-22 | As a User, I should be able to practice drawing illustrations on the Game Board, so that I can learn how to illustrate areas of the Game  Board. |
| MT-EPIC-7 | MT-STORY-23 | As a User, I should be able to practice rolling dice in the chat, so that I can learn how to make normal dice rolls. |
| MT-EPIC-7 | MT-STORY-24 | As a User, I should be able to practice making inline rolls in the chat, so that I can learn how to make dice rolls with contextual  information such as weapons or other information. |
| MT-EPIC-7 | MT-STORY-25 | As a User, when viewing campaigns, I should always be able to view the Tutorial Campaign, so that I can learn how to use the Application  at any time should I become unsure. |
| MT-EPIC-8 | MT-STORY-26 | As a User, I should be able to create new campaigns, so that I can run Virtual Tabletop games for Player Users |
| MT-EPIC-8 | MT-STORY-27 | As a User, I should be able to set a name for my campaign, so that other players can differentiate it among other campaigns |
| MT-EPIC-8 | MT-STORY-28 | As a User, I should be able to set a description for my campaign, so that other players can view more information for my campaign when  listing campaigns |
| MT-EPIC-8 | MT-STORY-29 | As a User, I should be able to set an image for my campaign, so that other players can view a visual representation of the campaign |
| MT-EPIC-8 | MT-STORY-30 | As a User, there should be no limitation to the number of campaigns I can create |
| MT-EPIC-8 | MT-STORY-31 | As a User, I should become the Game Master User for a given campaign after creating it, so that others cannot change my campaign |
| MT-EPIC-9 | MT-STORY-32 | As a Game Master User, I should be able to edit a campaign that I am a Game Master User of, so that I can change details of that campaign |
| MT-EPIC-9 | MT-STORY-33 | As a Game Master User, I should be able to edit the name of the campaign, so that I can make amendments |
| MT-EPIC-9 | MT-STORY-34 | As a Game Master User, I should be able to edit the description of the campaign, so that I can make amendments |
| MT-EPIC-9 | MT-STORY-35 | As a Game Master User, I should be able to edit the image for the campaign, so that I can make amendments |
| MT-EPIC-10 | MT-STORY-36 | As a User, I must be able to return back to a list of campaigns, so that I can switch to another campaign if I wish |
| MT-EPIC-11 | MT-STORY-37 | As a Game Master User, I should be able to view options for a campaign which are reserved only for Game Master Users: |
| MT-EPIC-11 | MT-STORY-38 | As a Game Master User, I should be able to restrict manipulation of Grid Controls, so that only Game Master Users can view the Game  Board Control options |
| MT-EPIC-11 | MT-STORY-39 | As a Game Master User, I should be able to restrict manipulation of Fog Of War, so that only Game Master Users can view Fog Of War  options |
| MT-EPIC-11 | MT-STORY-40 | As a Game Master User, I should be able to toggle restriction of tokens, so that only Game Master Users can view hidden tokens |
| MT-EPIC-11 | MT-STORY-41 | As a Game Master User, I should be able to set other players as a Game Master, so that they can become a game master |
| MT-EPIC-11 | MT-STORY-42 | As a Game Master User, I should be able to toggle usage of the "Move All Users" tool, so that only Dungeon Masters can move all users  between maps |
| MT-EPIC-12 | MT-STORY-43 | As a User, I require the ability to view a grid which is overlaid on the map so that I can judge the scale of the map |
| MT-EPIC-12 | MT-STORY-44 | As a Game Master User, I must be able to change the colour of a map grid outline, so that I can distinguish the Game Board from the  images in the map |
| MT-EPIC-12 | MT-STORY-45 | As a Game Master User, I must be able to change the fullness of the Game Board, so I can show a grid from a range of plus-shapes, to a  fully-formed grid. |
| MT-EPIC-12 | MT-STORY-46 | As a Game Master User, I must be able to change the default size of a grid square within the map, so I can resize the Game Board to fit  any images I have. |
| MT-EPIC-12 | MT-STORY-47 | As a Game Master User, I must be able to enable the snapping of Tokens to the grid, so that tokens fit exactly within the dimensions I  have specified for grid sqares. |
| MT-EPIC-12 | MT-STORY-48 | As a Game Master User, when I drag Tokens into the Game Board, Tokens must snap to the grid square which is closest in distance from the center-point of the token, so that the token always snaps to a given grid square. |
| MT-EPIC-12 | MT-STORY-49 | As a Game Master User, I must be able to disable the snapping of Tokens to the grid, so that tokens can be placed arbitrarily in any  position |
| MT-EPIC-12 | MT-STORY-50 | As a Player User, I must not be able to change this setting unless the Game Master User specifies I can, so that the specified play  style of the Game Master is correctly maintained. |
| MT-EPIC-12 | MT-STORY-51 | As a Game Master User, I must be able to perform calibration "grid find", to help me match the dimensions of the Grid to the images I  have uploaded into the Map |
| MT-EPIC-13 | MT-STORY-52 | As a User, I must be able to draw on the Game Board, to make illustrations to other Users |
| MT-EPIC-13 | MT-STORY-53 | As a User, I must be able to change the colour of the drawing I make on the Game Board, so that it can be distinguished more easily from the background image |
| MT-EPIC-13 | MT-STORY-54 | As a User, I must be able to change the size of the brush when drawing, so that my drawings have more or less detail |
| MT-EPIC-13 | MT-STORY-55 | As an Application, I must clear drawings from the Game Board after a short period of time, so the Game Board is not too cluttered.  |
| MT-EPIC-14 | MT-STORY-56 | As a Game Master, I must be able to obscure elements of the Game Board so that I can hide surprises from Player Users |
| MT-EPIC-14 | MT-STORY-57 | As a Game Master User, I must be able to obscure elements in an "additive" way, so that the entire Game Board is displayed with the  exception of area(s) I specify, so that I can obscure specific areas of the Game Board |
| MT-EPIC-14 | MT-STORY-58 | As a Game Master User, I must be able to obscure elements in a "subtractive" way, so that the entire Game Board is obscured, with the  exception of area(s) I specify, so that I can obscyure specific areas of the Game Board |
| MT-EPIC-14 | MT-STORY-59 | As a Game Master User, I must be able to toggle to obscure the entire game board, so that I can restart my "subtractive" way of  obscuring Game Board elements. |
| MT-EPIC-14 | MT-STORY-60 | As a Game Master User, I must be able to toggle to reveal the entire game board, so that I can restart my "additive" way of obscuring  Game Board elements or undo any obscuring of the Game Board. |
| MT-EPIC-15 | MT-STORY-61 | As a User, the most prominent area of the display when playing must be the Game Board, which displays maps, visual elements (such as  objects and images), characters, so that I can play Virtual Tabletop (VTT) games. |
| MT-EPIC-16 | MT-STORY-62 | As an Application, I should provide "maps" which are containers outlining how objects and images will be displayed on the Game Board |
| MT-EPIC-16 | MT-STORY-63 | As a Game Master User, I must be able to add new maps to the campaign, so that I can add images and tokens to that map, which will  appear on the Game Board |
| MT-EPIC-16 | MT-STORY-64 | As a Game Master User, I must be able to give my new map a name, so that I can distinguish it from other maps |
| MT-EPIC-16 | MT-STORY-65 | As a Game Master User, I must be able to add notes to my map, so that I can make note of its characteristics |
| MT-EPIC-16 | MT-STORY-66 | As a Game Master User, I must be able to set a map as hidden or displayed to Player Users, so that I can keep certain elements of the  game hidden for artistic effect.
| MT-EPIC-16 | MT-STORY-67 | As a Game Master User, I must be able to drag images from my device into the browser and into a specified area of the interface, so that I can add those images into the map Game Board |
| MT-EPIC-17 | MT-STORY-68 | As a Game Master User, I must be able to add multiple images to a map in a "Map Edit Mode", for visual effect and so that maps have a  visual representation of an area in the Game Board |
| MT-EPIC-17 | MT-STORY-69 | As a Game Master User, I must be able to click & drag to position images added to a map and displayed on the Game Board, for visual  effect |
| MT-EPIC-17 | MT-STORY-70 | As a Game Master User, I must be able to position images within a map in a layered way, so that images may be above or below eachother  on the Game Board, for visual effect |
| MT-EPIC-17 | MT-STORY-71 | As a Game Master User, I must be able to click and select images I have positioned and imported within the map and displayed on the Game Board, so that I have a visual acknowledgement that I am performing an action on a given map. |
| MT-EPIC-17 | MT-STORY-72 | As a Game Master User, I must be able to, when selecting an image, move that image above others, so that it appears on top of other  images on the Game Board |
| MT-EPIC-17 | MT-STORY-73 | As a Game Master User, I must be able to, when selecting an image, move that image below others, so that it appears below of other  images on the Game Board |
| MT-EPIC-17 | MT-STORY-74 | As a Game Master User, I must be able to, when selecting an image, delete that image so that it is removed from the map and not  displayed on the Game Board |
| MT-EPIC-17 | MT-STORY-75 | As a Game Master User, I must be able to de-select an image, so that I have a visual acknowledgement that I am not performing an action  on that element |
| MT-EPIC-17 | MT-STORY-76 | As a Game Master User, when I add a new map, I must be prompted with a "Map Edit Mode", so that I can edit the images displayed in that  map and on the Game Board |
| MT-EPIC-17 | MT-STORY-77 | As a Game Master User, I must be able to exit "Map Edit Mode" and return to viewing the map. |
| MT-EPIC-17 | MT-STORY-78 | As a Game Master User, when not in "Map Edit Mode", I must be able to return to "Map Edit Mode", so that I can make amendments to images and objects displayed in that map. |
| MT-EPIC-17 | MT-STORY-79 | As a Game Master User, I need the order and arrangement of the images and objects on the map and the Game Board to be preserved  throughout page refreshes, so that I can rely on the display as I position other elements on top of it. |
| MT-EPIC-18 | MT-STORY-80 | As a User, I must be able to view the map on the Game Board as it was preserved by the Game Master User, so that I get a correct visual  representation as intended by the Game Master. |
| MT-EPIC-18 | MT-STORY-81 | As a User, I must be able to pan the map with the use of the left-mouse click, so that I can move the Game Board and view objects and visual elements displayed on it. |
| MT-EPIC-18 | MT-STORY-82 | As a User, I must be able to zoom out of the map with the use of the scroll button moved downwards, so that I can see more elements or  view the map in overview |
| MT-EPIC-18 | MT-STORY-83 | As a User, I must be able to zoom into the map with the use of the scroll button moved upwards, so that I can see focus on fewer  elements or view the map in more detail |
| MT-EPIC-18 | MT-STORY-84 | As a User, I must be limited in the extent I can zoom out, so that the image display does not become too small and difficult to discern |
| MT-EPIC-18 | MT-STORY-85 | As a User, I must be limited in the extent I can zoom in, to reduce the likelihood of viewing pixelated or blurred images, and to make  viewing the Game Board easier |
| MT-EPIC-18 | MT-STORY-86 | As a User, my scroll or zoom actions must apply only to my session, and not be applied to other Users in the game, so that I do not  affect the playability of another person's session |
| MT-EPIC-19 | MT-STORY-87 | As a Game Master, I must be able to move all players between maps, so that I can transfer users between various parts of the Game World |
| MT-EPIC-20 | MT-STORY-88 | As a User, when viewing the Game Board, I must be able to view controls so that I can view my account details as they are provided by  the identity provider, so that I can review my account information
| MT-EPIC-20 | MT-STORY-89 | As a User, when logged in as a guest, I must be able to change my randomly-assigned name, so that I can express my individuality. |
| MT-EPIC-20 | MT-STORY-90 | As a User, when logged in as a guest, my name must not be restricted as unique, so that I can express my individuality without limits |
| MT-EPIC-20 | MT-STORY-91 | As a User, when logged in I must be able to view a button allowing me to log out, so I am aware I can log out |
| MT-EPIC-20 | MT-STORY-92 | As a User, when logged in, I must be able to log out of the interface, so I can end my session |
| MT-EPIC-21 | MT-STORY-93 | As a User, I must be able to send chat messages to all users in the campaign, so that I can communicate with other Users |
| MT-EPIC-21 | MT-STORY-94 | As a User, I must be able to perform dice rolls in the chat, so others can see the outcome of dice rolls |
| MT-EPIC-22 | MT-STORY-95 | As a User, I must be able to type Standard Dice Roll notation into the chat and view the result of that dice roll in the chat, so I can  perform the core actions of gameplay for Virtual Tabletop (VTT) games (e.g. 2d10) |
| MT-EPIC-22 | MT-STORY-96 | As a User, I must be able to type Standard Dice Roll notations into the chat along with text, wrapped in brackets ([[1d6+10+2d6+2]]), and display the text provided as well as display the result of the roll, so that I can approximate various types of rolls within the interface
| MT-EPIC-22 | MT-STORY-97 | As a System, I must be designed agnostic to dice-roll systems and, so that multiple dice rolling systems can be implemented easily (e.g. Fudge, d10x, etc).
| MT-EPIC-23 | MT-STORY-98 | As a User, I must be provided with a visible element displaying common dice rolls, so that I can more easily roll common dice rolls |
| MT-EPIC-24 | MT-STORY-99 | As a Game Master User, I must be able to create Characters within the campaign, so that I am able to make visual representations of  Player Users, Non-Player Characters and Monsters. |
| MT-EPIC-24 | MT-STORY-100 | As a Game Master User, I must be provided with a "Wizard" option, to walk me through the creation of a character |
| MT-EPIC-24 | MT-STORY-101 | As a Game Master User, I must be able to provide a name for the Character, to distinguish tokens from one another |
| MT-EPIC-24 | MT-STORY-102 | As a Game Master User, I must be able to provide a description for the Character, to provide further context for a user |
| MT-EPIC-24 | MT-STORY-103 | As a Game Master User, I must be able to cancel without creating a Character, so that I can undo a mistake or reset my process |
| MT-EPIC-24 | MT-STORY-104 | As a Game Master User, I must be provided with a "Quick Upload" option, to allow me to upload images for quick use within the  Application |
| MT-EPIC-24 | MT-STORY-105 | As a Game Master User, I am required to upload an image to represent the character, so that others can see it represented visually on  the Game Board
| MT-EPIC-24 | MT-STORY-106 | As a Game Master User, I must see a display of the image I have uploaded after it has been processed, so I receive a visual  confirmation it has been uploaded |
| MT-EPIC-25 | MT-STORY-107 | As a User I must be able to apply a Token to a character, so it can be distinguished and viewed on the Game Boards |
| MT-EPIC-25 | MT-STORY-108 | As a User I must be able to apply a Token Border Style, so it can be visually distinguished |
| MT-EPIC-25 | MT-STORY-109 | As a User I must be able to select a "Coin" Border Style, which provides a circular outline with a border around the image I have  uploaded |
| MT-EPIC-25 | MT-STORY-110 | As a User when selecting the "Coin" Border Style, I must be shown an animation displaying the border style, so I can see how it is  applied to the image. |
| MT-EPIC-25 | MT-STORY-111 | As a User I must be able to select a "Tile" Border Style, which provides a square outline with a border around the image I have uploaded |
| MT-EPIC-25 | MT-STORY-112 | As a User when selecting the "Tile" Border Style, I must be shown an animation displaying the border style, so I can see how it is  applied to the image. |
| MT-EPIC-25 | MT-STORY-113 | As a User I must be able to select a "Circle" Border Style, which crops the image I have provided into a circular shape |
| MT-EPIC-25 | MT-STORY-114 | As a User I must be able to select a "Circle" Border Style, which crops the image I have provided into a square shape |
| MT-EPIC-25 | MT-STORY-115 | As a User I must be able to select a color for the token when using "Tile" or "Coin" Border Style, so that it can be visually  distinguished from the images on the Game Board |
| MT-EPIC-25 | MT-STORY-116 | As a User I must be provided with a default list of commonly-used colours, to reduce the time required for configuration of tokens |
| MT-EPIC-25 | MT-STORY-117 | As a User I must be able to add new colours to the list of colours, so I can apply more visually distinctive colour borders |
| MT-EPIC-25 | MT-STORY-118 | As a User, I must be able to select a size for the token displayed in grid square sizes, so I can have more visually distinctive/larger elements on the Game Board, and to differentiate scale |
| MT-EPIC-25 | MT-STORY-119 | As a User, I must be able to apply a range of icons from the RPG Awesome Icon Library, which are displayed in top-right-most position  relative to the token, to indicate a specific status for the user |
| MT-EPIC-25 | MT-STORY-120 | As a User I must be able to search for icons from the RPG Awesome Icon Libray which can be added to the Token, so that I can use all available icons |
| MT-EPIC-25 | MT-STORY-121 | As a Game Master User, I must be able to delete tokens from the Game Board, but they must remain within the Character Library, so that  I can re-use them. |
| MT-EPIC-25 | MT-STORY-122 | As a Game Master User, I must be able to delete tokens from the Character Library, so I can remove tokens I no longer want. |
| MT-EPIC-25 | MT-STORY-123 | As an Application, I must not remove the images used for these tokens from the storage of the Application |
| MT-EPIC-26 | MT-STORY-124 | As an Application, I must support integration with a third-party authentication provider shared with the client, so I can validate the  authentication of sessions using the software |
| MT-EPIC-27 | MT-STORY-125 | As an Application, I require a OpenAPI Specification, so that I can explain the contract I provide. |
| MT-EPIC-28 | MT-STORY-126 | As a Developer, I require a debug modes, so that I can more effectively debug the application and develop software |
| MT-EPIC-29 | MT-STORY-127 | As a System, I require a production mode, so that developer-specific and security-critical information can be obscured from the public |
| MT-EPIC-30 | MT-STORY-128 | As an Application, I must support the configurability of Google Image Storage, so that images can be stored in a Third Party storage  solution. |
| MT-EPIC-30 | MT-STORY-129 | As an Application, I must support configurability of Google Image Storage using a credential file, so that I can authenticate with  Google services. |
| MT-EPIC-31 | MT-STORY-130 | As an Application deployed in production context, I must restrict the origin of clients connecting to me, so that I can reduce MITM and XSS attacks on insecure hosts. |
| MT-EPIC-32 | MT-STORY-131 | As a Server Administrator, I must be able to configure different options for storage, so I can use Mythic Table how I see fit within my architecture |
| MT-EPIC-32 | MT-STORY-132 | As an Application, I must provide an in-memory option, so that Server Administrators can trial the use of the application without committing to architecture |
| MT-EPIC-32 | MT-STORY-133 | As an Application, I must provide a MongoDB option, so that Server Administrators can use a persistent MongoDB instance to store data for Mythic Table |
| MT-EPIC-32 | MT-STORY-134 | As an Application, I must support the use of Redis as an optional storage medium, so that socket data can be stored and re-played to connected Users |