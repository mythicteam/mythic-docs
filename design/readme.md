---
label: Design Documentation
icon: package
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

This section of the documentation outlines the design documentation, outlining the user stories and specification for those user stories. This is intended for a technical audience, and is here to ensure that we have understanding of which features we have, and have some documentation explaining how the software was designed and intended for use. 

## Roles
| Role name | Description |
|---|---|
| Project  | The Mythic Table project itself  |
| Application  |  The Mythic Table application itself  |
| Observer | Any person using the Application who is not yet using the system as a Player User or Game Master User. |
| User | Any person using the Application. Encompasses the Player User and Game Master User |
| Player User  | A person who is playing a Virtual Tabletop games using Mythic Table.  |
| Game Master User  | A person who is running a Virtual Tabletop game as a dungeon/game master using Mythic Table. |
| Server Administrator  | A person who runs and maintains an instance of Mythic Table for Player Users and Game Master Users |
| Developer  | Any person performing development work on Mythic Table in any capacity |
