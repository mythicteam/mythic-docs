---
label: Maps and Map Images
icon: git-pull-request-closed
order: 9
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As an Application, I should provide "maps" which are containers outlining how objects and images will be displayed on the Game Board
* As a Game Master User, I must be able to add new maps to the campaign, so that I can add images and tokens to that map, which will appear on the Game Board
    * As a Game Master User, I must be able to give my new map a name, so that I can distinguish it from other maps
    * As a Game Master User, I must be able to add notes to my map, so that I can make note of its characteristics
    * As a Game Master User, I must be able to set a map as hidden or displayed to Player Users, so that I can keep certain elements of the game hidden for artistic effect.
* As a Game Master User, I must be able to drag images from my device into the browser and into a specified area of the interface, so that I can add those images into the map Game Board