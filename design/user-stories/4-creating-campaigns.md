---
label: Creating Campaigns
icon: duplicate
order: 16
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
* As a User, I should be able to create new campaigns, so that I can run Virtual Tabletop games for Player Users
    * As a User, I should be able to set a name for my campaign, so that other players can differentiate it among other campaigns
    * As a User, I should be able to set a description for my campaign, so that other players can view more information for my campaign when listing campaigns
    * As a User, I should be able to set an image for my campaign, so that other players can view a visual representation of the campaign
* As a User, there should be no limitation to the number of campaigns I can create
* As a User, I should become the Game Master User for a given campaign after creating it, so that others cannot change my campaign