---
label: Game Master Controls
icon: report
order: 14
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a Game Master User, I should be able to view options for a campaign which are reserved only for Game Master Users:
    * As a Game Master User, I should be able to restrict manipulation of Grid Controls, so that only Game Master Users can view the Game Board Control options
    * As a Game Master User, I should be able to restrict manipulation of Fog Of War, so that only Game Master Users can view Fog Of War options
    * As a Game Master User, I should be able to toggle restriction of tokens, so that only Game Master Users can view hidden tokens
    * As a Game Master User, I should be able to set other players as a Game Master, so that they can become a game master
    * As a Game Master User, I should be able to toggle usage of the "Move All Users" tool, so that only Dungeon Masters can move all users between maps