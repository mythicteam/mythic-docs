---
label: Account View Mode
icon: person
order: 5
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a User, when viewing the Game Board, I must be able to view controls so that I can view my account details as they are provided by the identity provider, so that I can review my account information
* As a User, when logged in as a guest, I must be able to change my randomly-assigned name, so that I can express my individuality.
    * As a User, when logged in as a guest, my name must not be restricted as unique, so that I can express my individuality without limits
* As a User, when logged in I must be able to view a button allowing me to log out, so I am aware I can log out
* As a User, when logged in, I must be able to log out of the interface, so I can end my session