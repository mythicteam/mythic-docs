---
label: Map Move-Player Mode
icon: git-compare
order: 6
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a Game Master, I must be able to move all players between maps, so that I can transfer users between various parts of the Game World
