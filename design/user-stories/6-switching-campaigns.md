---
label: Switching campaigns
icon: arrow-switch
order: 15
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a User, I must be able to return back to a list of campaigns, so that I can switch to another campaign if I wish