---
label: Fog of war
icon: square-fill
order: 11
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
* As a Game Master, I must be able to obscure elements of the Game Board so that I can hide surprises from Player Users
    * As a Game Master User, I must be able to obscure elements in an "additive" way, so that the entire Game Board is displayed with the exception of area(s) I specify, so that I can obscure specific areas of the Game Board
    * As a Game Master User, I must be able to obscure elements in a "subtractive" way, so that the entire Game Board is obscured, with the exception of area(s) I specify, so that I can obscyure specific areas of the Game Board
    * As a Game Master User, I must be able to toggle to obscure the entire game board, so that I can restart my "subtractive" way of obscuring Game Board elements.
    * As a Game Master User, I must be able to toggle to reveal the entire game board, so that I can restart my "additive" way of obscuring Game Board elements or undo any obscuring of the Game Board.
