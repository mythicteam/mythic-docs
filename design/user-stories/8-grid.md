---
label: Grid display
icon: hash
order: 13
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
* As a User, I require the ability to view a grid which is overlayed on the map so that I can judge the scale of the map
* As a Game Master User, I must be able to change the colour of a map grid outline, so that I can distinguish the Game Board from the images in the map
* As a Game Master User, I must be able to change the fullness of the Game Board, so I can show a grid from a range of plus-shapes, to a fully-formed grid.
* As a Game Master User, I must be able to change the default size of a grid square within the map, so I can resize the Game Board to fit any images I have.
    * As a Game Master User, I must be able to enable the snapping of Tokens to the grid, so that tokens fit exactly within the dimensions I have specified for grid sqares.
        * As a Game Master User, when I drag Tokens into the Game Board, Tokens must snap to the grid square which is closest in distance from the center-point of the token, so that the token always snaps to a given grid square.
        * As a Game Master User, I must be able to disable the snapping of Tokens to the grid, so that tokens can be placed arbitrarily in any position
        * As a Player User, I must not be able to change this setting unless the Game Master User specifies I can, so that the specified play style of the Game Master is correctly maintained.
* As a Game Master User, I must be able to perform calibrational "grid find", to help me match the dimensions of the Grid to the images I have uploaded into the Map
