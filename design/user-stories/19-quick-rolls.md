---
label: Quick rolls
icon: package
order: 3
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
* As a User, I must be provided with a visible element displaying common dice rolls, so that I can more easily roll common dice rolls