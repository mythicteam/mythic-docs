---
label: Drawing
icon: pencil
order: 12
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a User, I must be able to draw on the Game Board, to make illustrations to other Users
    * As a User, I must be able to change the colour of the drawing I make on the Game Board, so that it can be distinguished more easily from the background image
    * As a User, I must be able to change the size of the brush when drawing, so that my drawings have more or less detail
    * As an Application, I must clear drawings from the Game Board after a short period of time, so the Game Board is not too cluttered.