---
label: Editing campaigns
icon: pencil
order: 16
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a Game Master User, I should be able to edit a campaign that I am a Game Master User of, so that I can change details of that campaign
    * As a Game Master User, I should be able to edit the name of the campaign, so that I can make amendments
    * As a Game Master User, I should be able to edit the description of the campaign, so that I can make amendments
    * As a Game Master User, I should be able to edit the image for the campaign, so that I can make amendments