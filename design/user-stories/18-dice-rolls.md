---
label: Dice rolls
icon: package
order: 3
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
* As a User, I must be able to type Standard Dice Roll notation into the chat and view the result of that dice roll in the chat, so I can perform the core actions of gameplay for Virtual Tabletop (VTT) games (e.g. 2d10)
* As a User, I must be able to type Standard Dice Roll notations into the chat along with text, wrapped in brackets ([[1d6+10+2d6+2]]), and display the text provided as well as display the result of the roll, so that I can approximate various types of rolls within the interface
* As a System, I must be designed agnostic to dice-roll systems and, so that multiple dice rolling systems can be implemented easily (e.g. Fudge, d10x, etc).