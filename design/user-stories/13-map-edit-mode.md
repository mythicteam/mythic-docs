---
label: Map edit mode
icon: pencil
order: 8
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a Game Master User, I must be able to add multiple images to a map in a "Map Edit Mode", for visual effect and so that maps have a visual representation of an area in the Game Board
* As a Game Master User, I must be able to click & drag to position images added to a map and displayed on the Game Board, for visual effect
* As a Game Master User, I must be able to position images within a map in a layered way, so that images may be above or below eachother on the Game Board, for visual effect
* As a Game Master User, I must be able to click and select images I have positioned and imported within the map and displayed on the Game Board, so that I have a visual acknowledgement that I am performing an action on a given map.
    * As a Game Master User, I must be able to, when selecting an image, move that image above others, so that it appears *on top* of other images on the Game Board
    * As a Game Master User, I must be able to, when selecting an image, move that image below others, so that it appears *below* of other images on the Game Board
    * As a Game Master User, I must be able to, when selecting an image, delete that image so that it is removed from the map and not displayed on the Game Board
* As a Game Master User, I must be able to de-select an image, so that I have a visual acknowledgement that I am not performing an action on that element
* As a Game Master User, when I add a new map, I must be prompted with a "Map Edit Mode", so that I can edit the images displayed in that map and on the Game Board
* As a Game Master User, I must be able to exit "Map Edit Mode" and return to viewing the map.
* As a Game Master User, when not in "Map Edit Mode", I must be able to return to "Map Edit Mode", so that I can make amendments to images and objects displayed in that map.
* As a Game Master User, I need the order and arrangement of the images and objects on the map and the Game Board to be preserved throughout page refreshes, so that I can rely on the display as I position other elements on top of it.
