---
label: Character Library
icon: person
order: 2
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a Game Master User, I must be able to create Characters within the campaign, so that I am able to make visual representations of Player Users, Non-Player Characters and Monsters.
* As a Game Master User, I must be provided with a "Wizard" option, to walk me through the creation of a character
    * As a Game Master User, I must be able to provide a name for the Character, to distinguish tokens from one another
    * As a Game Master User, I must be able to provide a description for the Character, to provide further context for a user
    * As a Game Master User, I must be able to cancel without creating a Character, so that I can undo a mistake or reset my process
* As a Game Master User, I must be provided with a "Quick Upload" option, to allow me to upload images for quick use within the Application
* As a Game Master User, I am required to upload an image to represent the character, so that others can see it represented visually on the Game Board
    * As a Game Master User, I must see a display of the image I have uploaded after it has been processed, so I receive a visual confirmation it has been uploaded