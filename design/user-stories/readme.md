---
label: Epics & User Stories
icon: rocket
order: 1
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

[!ref text="0 - User Login"](0-user-authentication.md)

Logging into the user interface, and how the Application behaves with the third-party authentication provider.

[!ref text="1 - First Run"](1-first-run.md)

Things that happen after the user has logged in, and before the user has full use of the application.

[!ref text="2 - Finding and Joining Games"](2-finding-joining-games.md)

After the user has logged in, and when the user is looking for available games.

[!ref text="3 - The Tutorial Campaign"](3-tutorial-campaign.md)

What the tutorial campaign covers, which shows the Users how to use the Application.

[!ref text="4 - Creating Campaigns"](4-creating-campaigns.md)
When the user is creating a new campaign.

[!ref text="5 - Editing Campaigns"](5-editing-campaigns.md)
When the user is editing a campaign (changing name and description, image).

[!ref text="6 - Switching Campaigns"](6-switching-campaigns.md)
When the user is currently in a campaign and wants to switch between that campaign and another.

[!ref text="7 - Game Master User Controls"](7-game-master-user-controls.md)
When a Game Master wants to change the accessibility of various Game Master-only functions.

[!ref text="8 - Grid"](8-grid.md)
The overlay of the grid on top of the images displayed on the Game Board.

[!ref text="9 - Drawing"](9-drawing.md)
Feature allowing Users to draw on the Game Board to provide illustrations.

[!ref text="10 - Fog of War"](10-fog-of-war.md)
Feature allowing Game Masters to limit visibility of areas of the Game Board.

[!ref text="11 - Game Board"](11-game-board.md)
The main display of the Application - where the user moves tokens, adds objects, draws, etc.

[!ref text="12 - Maps and Map Images"](12-maps-and-map-images.md)
Adding of images into maps and positioning of map images, layering, etc.

[!ref text="13 - Map Edit Mode"](13-map-edit-mode.md)
The addition of images into the Map and Game Board, sizing, and calibration of map position.

[!ref text="14 - Map View Mode"](14-map-view-mode.md)
Viewing the map that on the Game Board.

[!ref text="15 - Map Move-Player Mode"](15-map-move-player-mode.md)
Where the Game Master moves players between maps.

[!ref text="16 - Account View Mode"](16-account-view-mode.md)
Where the Game Master moves players between maps.

[!ref text="17 - Chat"](17-chat.md)
Where users communicate with eachother and roll dice.

[!ref text="18 - Dice rolls"](18-dice-rolls.md)
Where users roll dice on and in the user interface.

[!ref text="19 - Quick rolls"](19-quick-rolls.md)
Where the user can roll common dice rolls.

[!ref text="20 - Character library"](20-character-library.md)
Where Game Masters can drag characters onto the Game Board.

[!ref text="21 - Tokens"](20-character-library.md)
Where Users can change the appearance of a character when displayed on the Game Board.
