---
label: The Game Board
icon: flame
order: 10
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
* As a User, the most prominent area of the display when playing must be the Game Board, which displays maps, visual elements (such as objects and images), characters, so that I can play Virtual Tabletop (VTT) games.
