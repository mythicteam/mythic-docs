---
label: Chat
icon: comment
order: 4
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a User, I must be able to send chat messages to all users in the campaign, so that I can communicate with other Users
* As a User, I must be able to perform dice rolls in the chat, so others can see the outcome of dice rolls