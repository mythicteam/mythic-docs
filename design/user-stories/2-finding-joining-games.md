---
label: Finding & joining games
icon: codescan-checkmark
order: 18
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
* As a User, I must be able to view a list of games that are available to me, so that I can join games I want to play.
