---
label: The Tutorial Campaign
order: 17
icon: mortar-board
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As an Application, I should provide a tutorial campaign to Users, so that they can familiarise themselves with gameplay in the Application.
    * As a User, I should be able to practice clicking and dragging an character token, so that I can learn how to move characters around the Game Board.
    * As a User, I should be able to practice panning and zooming the Game Board, so that I can learn how to view the game environment.
    * As a User, I should be able to practice switching between multiple maps within a campaign, so that I can learn how to navigate maps within my campaign.
    * As a User, I should be able to practice adding new maps within a campaign, so that I can learn how to expand my campaigns.
    * As a User, I should be able to practice deleting character tokens within a campaign, so I can learn how to clean up campaigns.
    * As a User, I should be able to practice drawing illustrations on the Game Board, so that I can learn how to illustrate areas of the Game Board.
    * As a User, I should be able to practice rolling dice in the chat, so that I can learn how to make normal dice rolls.
    * As a User, I should be able to practice making inline rolls in the chat, so that I can learn how to make dice rolls with contextual information such as weapons or other information.
* As a User, when viewing campaigns, I should always be able to view the Tutorial Campaign, so that I can learn how to use the Application at any time should I become unsure.