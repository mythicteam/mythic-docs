---
label: User Login Epics
icon: codespaces
order: 20
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

# Allow a user to log into the application
* As a Visitor, I must be able to access the login page of the application, so that I can authenticate
## Logging in
* As an Application, I require user authentication which requires users to authenticate to use the application via username and password so that Users can control access to their games.
    * As an Application, I require the process of authentication (checking, storage, and validation of user credentials) to be handled by an external provider, to reduce technical complexity.
    * As an Application, I must abide by the registration policies of the authentication as set by the external provider, so that the policy set by the Server Administrator is applied correctly.
        * As an Application, I must allow "Guest Users" to use the interface without need for a username and/or password, so that users may use me without the need to register via the external provider.
            * As a guest User, I require a randomly-allocated, unique, and human-readable name, so that other players can identify me uniquely.
        * As a Visitor, if the policies of the external provider allow it, I must be able to register for a new account.
        * As a User, I must be required to re-authenticate if my token expires, so that the possibility of my account or credentials being used without my knowledge is minimised.
        * As an Application, I must not display the user interface to users who have failed to authenticate according to the policies set by the external provider, so that the policy set by the Server Administrator is applied correctly.
    * As an Application, I require the use of token-based authentication when a User is using the application, so that Users can control access to their games.
    * As a User, I must be able to request a "forgotten password" via the interface, so that if I forget my password, I can follow the process set by the external provider and regain access to my account
    * As a User, I must be able to request "remember me" so that if I close my browser and return to the Application, I will retain my session provided that it is not expired, so that I do not need to repeatedly log into the application.