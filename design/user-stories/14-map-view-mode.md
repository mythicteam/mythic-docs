---
label: Map View Mode
icon: eye
order: 7
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a User, I must be able to view the map on the Game Board as it was preserved by the Game Master User, so that I get a correct visual representation as intended by the Game Master.
* As a User, I must be able to pan the map with the use of the left-mouse click, so that I can move the Game Board and view objects and vidual elements displayed on it.
* As a User, I must be able to zoom out of the map with the use of the scroll button moved downwards, so that I can see more elements or view the map in overview
* As a User, I must be able to zoom into the map with the use of the scroll button moved upwards, so that I can see focus on fewer elements or view the map in more detail
* As a User, I must be limited in the extent I can zoom out, so that the image display does not become too small and difficult to discern
* As a User, I must be limited in the extent I can zoom in, to reduce the likelihood of viewing pixelated or blurred images, and to make viewing the Game Board easier
* As a User, my scroll or zoom actions must apply only to my session, and not be applied to other Users in the game, so that I do not affect the playability of another person's session
