---
label: Tokens
icon: person
order: 1
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a User I must be able to apply a Token to a character, so it can be distinguished and viewed on the Game Boards
    *  As a User I must be able to apply a Token Border Style, so it can be visually distinguished:
        * As a User I must be able to select a "Coin" Border Style, which provides a circular outline with a border around the image I have uploaded
            * As a User when selecting the "Coin" Border Style, I must be shown an animation displaying the border style, so I can see how it is applied to the image.
        * As a User I must be able to select a "Tile" Border Style, which provides a square outline with a border around the image I have uploaded
            * As a User when selecting the "Tile" Border Style, I must be shown an animation displaying the border style, so I can see how it is applied to the image.
        * As a User I must be able to select a "Circle" Border Style, which crops the image I have provided into a circular shape
        * As a User I must be able to select a "Circle" Border Style, which crops the image I have provided into a square shape
    * As a User I must be able to select a color for the token when using "Tile" or "Coin" Border Style, so that it can be visually distinguished from the images on the Game Board
        * As a User I must be provided with a default list of commonly-used colours, to reduce the time required for configuration of tokens 
        * As a User I must be able to add new colours to the list of colours, so I can apply more visually distinctive colour borders
    * As a User, I must be able to select a size for the token displayed in grid square sizes, so I can have more visually distinctive/larger elements on the Game Board, and to differentiate scale
    * As a User, I must be able to apply a range of icons from the RPG Awesome Icon Library, which are displayed in top-right-most position relative to the token, to indicate a specific status for the user
        * As a User I must be able to search for icons from the RPG Awesome Icon Libray which can be added to the Token, so that I can use all available icons
* As a Game Master User, I must be able to delete tokens from the Game Board, but they must remain within the Character Library, so that I can re-use them.
* As a Game Master User, I must be able to delete tokens from the Character Library, so I can remove tokens I no longer want.
    * As an Application, I must not remove the images used for these tokens from the storage of the Application