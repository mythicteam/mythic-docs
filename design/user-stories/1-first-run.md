---
label: First run 
icon: north-star
order: 19
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

* As a User, I should be shown a display showing features available in the Application, so that I can review ways I can use the application, be aware of the state of development, and view social media links for the Mythic Table project.
