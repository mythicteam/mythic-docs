---
icon: number
expanded: true
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

## Versions
| Version   | Name  | Release date  | Link |
|---|---|---|---|
| 0.11.0 | "First Playable"  |  ?  | [Docs](./V0.11.0)  |