---
label: Mythic Table v0.11.0 ("First Playable") Docs
icon: repo
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
![Mythic Table](../../../images/mythic_logo.png)
Welcome to the Mythic Table documentation for version 0.1! 

## About this version
This version is referred to within Mythic Table as "First Playable". The First Playable was a goal set to get us to the first version of Mythic Table which was playable for the community. This does not mean that it is fully playable. Think of it as the "Minimum Viable Project" (MVP).

## Manuals
[!ref icon="person" text="Player's manual"](players-manual/readme.md)
A guide for using Mythic Table as a regular player. This includes how to use mythic table to play a Virtual Tabletop game.

[!ref icon="person-fill" text="Game Master's manual"](game-masters-manual/readme.md)
A guide for using Mythic Table as a Game Master. This includes how to set up a Virtual Tabletop game.

[!ref icon="tools" text="Server Administrator's manual"](server-admins-manual/readme.md)
Technical setup information for server owners wanting to host Mythic Table on their server.