---
label: Character macros (monster attack rolls)
order: -2
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

![An example of the types of macro you can create](./../../images/dm-character-macros.png)
Macros allow you to create a list of actions which can be clicked when viewing the character/monster/NPC. In this release, this mainly allows you to send text to the chat box, which allows you to:
* Say text in the chatbox as that token
* Send inline rolls to the chatbox with text (to mimic weapon and attack rolls)
* Send regular dice rolls to the chatbox (to show the full roll information)

### Using macros 
!!!warning Bug!
There are a few bugs around this area. If attempting to enter HTML markup or any special characters, the token will not save. In addition if the user attempts to change an inline dice roll to a regular one with text, the token will be deleted.
!!!
To execute these, double-click the icon in the grid/map to display the character dialog. It will then be input to the chat on the right-hand side.

### Inline rolls (mimic attack rolls, etc)
You can mimic attack rolls by creating a macro with both text and a dice roll. For instance, a macro with the content:
![An example attack roll macro](./../../images/dm-macros-attack.png)

Will show:
![The result of the above macro](./../../images/dm-character-macro-click-attack-roll.png)

### Regular rolls
!!!warning Bug!
If attempting to use text along with a regular roll, your roll will not be recognised.
* **Works**: ```1d20``` (Will show the roll result in chat)
* **Does not work**: ```OMG a 1d20``` (Will show ```OMG a 1d20``` without a result)
!!!

You can mimic also regular rolls using macros which include just a dice roll. For instance, a macro with the content:
![A regular roll (without any text included)](./../../images/dm-macros-regular-roll.png)

Will show:
![A regular roll (without any text included)](./../../images/dm-macros-regular-roll-output.png)
