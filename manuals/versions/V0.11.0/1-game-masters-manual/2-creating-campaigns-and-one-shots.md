---
label: Creating campaigns and one-shots
order: 1
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

![A list of games available, and the create button](./../images/player-list-campaigns.png)
Starting a campaign on Mythic Table is pretty easy. Once you have logged in, you will be given a list of existing campaigns.

### Creating a new campaign
!!!warning Bug!
Note: In this release, if you create a campaign and then want to edit the title or description, you'll encounter an error but won't see it on the UI. This has been fixed after V0.11.0.
!!!

![Creating a new campaign](./../images/dm-create-campaign-dialog.png)

When creating a new campaign, click on the large "+" icon to the right of the list and you'll be provided with a dialogue for creating a new campaign with the following fields:
* **Name** - The name of the campaign that will be displayed on the list of campaigns, and on the invitation players see before starting the game.
* **Description** - The description of the campaign which will be displayed on the invitation players see before starting the game.
* **Image** - Drag images onto the area displayed, or click the area to upload a picture. The image will be displayed on the list of campaigns and in the invitation players see.

![The campaign displayed in the list after it has been created](./../images/dm-new-campaign-created.png)

## Sharing the campaign with your players
To share the campaign with your players, once you have created the campaign, locate the campaign you have created and move your mouse over your campaign item, and click the link "info".

![The 'info' button displayed when moving the mouse over the campaign item](./../images/dm-new-campaign-info.png)
![The 'info' button displayed when moving the mouse over the campaign item](./../images/dm-create-campaign-info-dialog.png)

Once you see the information for the campaign, clicking on the "link" button (in red) will copy the link for your campaign into your clipboard. You can paste this link by using **Ctrl+V** into whichever application you are using to communicate with your players.

![The copied link for the game](./../images/dm-new-campaign-info-copied.png)