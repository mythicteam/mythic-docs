---
label: Manipulating/Moving tokens
order: -2
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

In this release, the Game Master (GM) can move tokens around for players as well as for monsters. 

### Moving characters between maps
!!!warning Missing feature
If you want to move only specific user to another map, this is not available in this release. To work around this, 
you will need to ask the player to click the map from the list on the right-hand-side themselves.
!!!

![Moving all characters using the 'Move All' button](./../../images/dm-move-characters.png)

To move characters between maps, move your mouse over the map on the right-hand side and click "Move all". This will move all users to the selected map.

### Marking tokens as dead
In this release, the easiest way of doing this is by selecting a token with the left-click, and then double-clicking it to view its properties. You can add an icon which notes something is dead by searching for "skull". The software currently does not support full-width labels (e.g. an "X" over the token):
![Marking a token as dead](./../../images/dm-mark-characters-as-dead.png)