---
label: Missing features in V0.11.0
order: 2
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

So, we're an open source project made up of volunteers. With this release we are at the point of having a "first playable" which means that you can use Mythic Table to run a game. However, some of the features you might expect from other Virtual Tabletop (VTT) systems, are not present in Mythic Table, yet. We're working on it! 

### Feature table
| Feature      | Workaround | Status      |
| ----------- | ----------- | ----------- |
| Searching/filtering campaigns   | Use search feature in browser | Unknown |
| Compendium   | None | Planned (Journal) |
| Character sheets    | None | Unknown |
| Contextual help    | None | Unknown |
| Background music    | Play music through other software (e.g. Voicemeeter, discord, etc) | Unknown |
| 3D/2D Dice     | None | Unknown |
| Turn tracker / Initiative mode     | Manual turn-tracking using other software or pen & paper | Unknown |
| Ruler     | Manual measurement is required by eye | Unknown |
| Journal     | Use separate window with documentation, type rolls into chat | Planned |
| DM-only dice rolls      | Use online dice roller or discord channel separate to Mythic Table game.       | Unknown |
| Limit token movement to player      | None      | Unknown |
| Automatic fog of war   | Manually draw Fog of War        | Unknown |
| Visual effects   | None       | Unknown |
| Character/Monster HP Display  | Maintain HPs outside of the Mythic Table instance (e.g. Spreadsheet)       | Unknown |
| Character/Monster HP Add/Remove  | Maintain HPs outside of the Mythic Table instance (e.g. Spreadsheet)       | Unknown |
| Monster sheets  | Use other websites or tools       | Planned (Journal) |
