---
label: Editing new & existing maps
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

### Importing images
!!!info Attribution
The images used for these examples are provided by Forgotten Maps from their Imgur page (https://imgur.com/gallery/ui96E4W#k8Hdy8U)
If you like them, consider supporting them!
!!!
When creating a map, you can upload multiple images to create a field for your players to play on. When you create a new map, you will also be automatically see the dialog to edit the map. To add an image, drag and drop it into the area provided at the bottom of the screen, or click the area at the bottom and upload an image. 
![Editing a map](./../../images/dm-editing-map-wizard.png)

## Editing the imported image in the map (grid) view
To edit the image, find the image in the list of maps to the right, and click the pen icon. This will bring up the "edit map" dialogue. To edit the image imported into the map, click on the "edit" button to the right

### Selecting a map image
To select a map while in edit mode, select the image, and an outline will appear. To de-select it, click the image again, and the image outline will disappear. While an image is selected, the below actions can be used to manipulate the map.

![Selecting a map. Ironmouse is a copyright of VShoujo and you should also watch her :P](./../../images/dm-edit-map-image-selected.png)

### Moving imported map images
![The grid display after a map is uploaded](./../../images/dm-uploaded-image.png)
When a map has been imported, you will see it displayed on the map/grid display. When clicking on the map, it will allow you to select it in "edit" mode so that you can resize it. If you are wanting to resize the map to align it with the grid, see the section below for "Grid Finder Mode".

### Edit map action bar
![The action bar in 'edit map' mode](./../../images/dm-edit-map-action-bar.png)

#### Send to back
![Send to back](./../../images/dm-edit-map-action-bar-bring-to-front.png)
Select the image with *left-mouse click* and then select the top-left icon displayed above. This will bring the image behind other images.

#### Move to top
![Bring to top](./../../images/dm-edit-map-action-bar-send-to-back.png)
Select the image with *left-mouse click* and then select the second-to-top-left icon displayed above. This will bring the image forward. 

#### Delete map
![Delete](./../../images/dm-edit-map-action-bar-delete.png)
To delete an imported image, first select the image with the *left-mouse click* and then either select the "delete" icon from the top action bar, or use the *del key* on your keyboard.

#### Close edit mode
![Close](./../../images/dm-edit-map-action-bar-close.png)
Clicking the "x" icon will close the edit map mode, and return back to the map grid display.
