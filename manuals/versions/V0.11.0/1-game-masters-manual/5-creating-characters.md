---
label: Creating characters
order: -1
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

!!!primary Missing features in this release
As a DM, you may be used to other features being available in other, more well-established Virtual Tabletop (VTT) systems, you may want to take a look at what common features are missing from this release in the link below:
* [FAQ - What is missing from this release?](1-faq-missing.md)
!!!

Characters are the basis of any roleplaying game, and you need them to be able to have players play on the Virtual Tabletop (VTT). As well as players, you need to be able to add monsters.

# Characters/monsters/NPCs
In this release of Mythic Table, characters, monsters and NPCs are displayed in the same way, and are shown the same way. 

## Creating character token images
Characters, monsters and NPCs are images which you upload and provide details for, just like any other Virtual Tabletop (VTT). With MythicTable, you need to upload your own images. Rounding of items is done in-app, so you don't need to use third party tools to do that for you.

## Creating new characters / monsters / NPCs
![The character display](./../../images/dm-character-display.png)
To create a new character to use in your campaign, you have two options:
* The character wizard
* Bulk upload

## Character wizard
![The button for the character wizard](./../../images/dm-character-create-wizard.png)

The character wizard is opened using the left-hand-side of the character display on the right hand side.

![Dialog shown for creating a new character image](./../../images/dm-character-create.png)

When clicking the button, you will be given a dialog to create the character, give it a name, and change various properties of the character. 

The tab **description** allows you to set visible properties 
* **Name** - The visible name for the character, monster or NPC you are creating.
* **Description** - Any additional information you want to show.
* **Macros** - Allows you to create macros which can be clicked on when viewing the character information (see section below for more info)

![Dialog shown for creating a new character image](./../../images/dm-character-create-wizard-token.png)

The tab **token** allows you to set specific properties for how the token will be shown

* **Style** - The shape and outline the token will have when displayed on the map/grid. Changing this changes the position of the icon relative to the token (i.e. moves it further vertically away from the token)
    * **Coin** - The default and most popular VTT style - a rounded outline.
    * **Tile** - A square border.
    * **Circle** - A circular icon with no border.
    * **Square** - A square icon with no border.
* **Color** - The colour of the outline displayed with the outline.
* **Size** - The size of the token in relation to a grid square:
    * **Tiny** - 0.5x0.5 grid squares
    * **Medium** - 1x1 grid square
    * **Large** - 2x2 grid squares
    * **Huge** - 3x3 grid squares
* **Icon** - Allows you to overlay an icon on top of the token to display a status, like death, etc. This uses RPG Awesome icons (https://nagoshiashumari.github.io/Rpg-Awesome/)

![A token with an icon](./../../images/dm-token-with-icon.png)

## Hiding tokens
You can hide tokens by double-clicking on the token and clicking the "Visible" button. This will toggle the visibility of the token.

# Bulk upload
Using the bulk upload will allow you to upload multiple images to use as tokens. They will be created with all the default options shown in the character wizard, and will import them with the name of each image provided.