---
label: Creating new maps
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

One of the first things any Game Master (GM) will want to do is to create a map which your players can move around on, and on which you can run your campaigns. 

## The default map
!!!info Note
If you want to use the default map, skip ahead to "editing maps", which goes into the detail of how to edit the default map and use it for your own purposes.
!!!

When creating a new campaign, there will always be one default map.


## Creating a map
To create a map, click on the "+" icon (circled in the image below) on the right-hand-side of the display.
![The 'create map' button (circled)](./../images/dm-add-new-map-button.png)
![The dialog displayed when creating a new map](./../images/dm-create-new-map.png)

The dialog that appears displays:
* **Name** - The name of the map. (Note: this is visible to players and DMs)
* **Notes** - Any notes you might want to put on that map (e.g. "TPK Here LOL")
* **Color** - The colour outline the map will have in the map list on the right-hand-side of the window.
* **Fullness** - ??
* **Size (Pixels)** - The size of each grid square in pixels on the map. This is used to size the grid to the size of an image which has a grid included.
