---
label: Mythic Table Game Master's Manual
icon: person-fill
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
![Mythic Table](./../../../images/mythic_logo.png)

### Welcome to Mythic Table!
Welcome! You're here because you want to know how to use Mythic Table, and this manual has got you covered. This manual is a part of the Mythic Table documentation, which is here to help you, the Game Master, on how to use Mythic Table. 

[!ref text="1 - Getting started"](0-getting-started.md)

Outlines how as a game master you can get started with Mythic Table.

[!ref text="2 - FAQ -  Missing features"](1-faq-missing.md)

Features that you'll be used to with other VTTs but which are missing thus far.

[!ref text="3 - Creating campaigns and one-shots"](2-creating-campaigns-and-one-shots.md)

How to create campaigns and one-shots you can share with your friends.

[!ref text="4 - Editing maps"](4-editing-maps.md)

How to upload images and use maps.

[!ref text="5 - Creating characters"](5-creating-characters.md)

How to create characters and tokens for players, NPCs and monsters.

[!ref text="6 - Macros"](6-macros.md)

A feature for recording common patterns and using them with tokens.

[!ref text="7 - Manipulating tokens"](7-manipulating-tokens.md)

How to mark tokens as dead, and how to move players between maps.

