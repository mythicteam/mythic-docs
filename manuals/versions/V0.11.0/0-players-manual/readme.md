---
label: Mythic Table Player's Manual
icon: person
---
![Mythic Table](./../../../images/mythic_logo.png)

### Welcome to Mythic Table!
Welcome! You're here because you want to know how to use Mythic Table, and this manual has got you covered. This manual is a part of the Mythic Table documentation, which is here to help you, the player, on how to use Mythic Table. 

Choose the section from the left-hand menu for the specific thing you want to get to know. If you just want to get started, then continue onwards!

[!ref text="1 - Getting started"](0-getting-started.md)

Outlines how as a player you can get started with Mythic Table.

[!ref text="2 - Logging in"](1-logging-in.md)

Gives details on logging in to Mythic Table.

[!ref text="3 - Joining games"](2-joining-games.md)

Shows you how you can browse for games, and join them via a join link.

[!ref text="4 - The User Interface"](user-interface/readme.md)

Goes through each element of the user interface and shows you what it does.
