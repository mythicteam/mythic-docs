---
label: Character/token display
order: 2
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
![The character list display](./../../images/player-characters-display.png)

The map display window is currently displayed or players, however, it is intended only for use by Game Masters to add character tokens onto the map. Consult your DM before using this feature, but it is recommended to avoid its use.