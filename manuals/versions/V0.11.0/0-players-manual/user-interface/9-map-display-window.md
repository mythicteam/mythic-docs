---
label: Map display window
order: 3
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
![The maps list display](./../../images/player-maps-display.png)

The map display window is currently displayed or players, however, it is intended only for use by Game Masters to list maps and move players between them. Consult your DM before using this feature, but it is recommended to avoid its use.