---
label: Dice roller toolbar
order: 5
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

The dice roller toolbar allows you to roll several dice rolls which are used extensively in tabletop games. 

![Dice rolls](./../../images/player-dice-rolls.png)
From left to right, these are:
1. D20 - Rolls value of 1d20 (Icosahedron)
2. D10 - Rolls value of 1d10 (Pentagonal trapezohedron)
3. D4 - Rolls a value of 1d4 (Pyramid)
4. D6 - Rolls a value of 1d6 (Cube)
5. D8 - Rolls a value of 1d8 (Regular octahedron)
6. D12 - Rolls a value of 1d12 (Dodecahedron)

When clicking on an individual dice item, it will display the roll in the chat display to the right-hand-side of the user interface.

![The chat displaying dice rolls after clicking the dice](./../../images/player-dice-rolls-chat.png)


### Notes
* You cannot roll more than one of the dice at once with this toolbar.