---
label: Fog of war tool
order: 10
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

![The Fog of War Tool](./../../images/player-fog-of-war-menu.png)

The Fog of War tool is a Dungeon-Master only tool that allows the Game Master to obscure parts of the map to heighten the surprise and hide elements from players that are meant to be displayed later in the order of the campaign or one-shot.

This feature is disabled for players.