---
label: Map/grid display
order: 1
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

The map/grid display is the bread and butter of the Virtual Table Top (VTT), and of Mythic Table. 

### Character tokens
![An example player token representing our very own fearless Marc!](./../../images/player-token.png)
When playing your campaign, your Game Master will allocate you a token for characters that you control. This allows you to move your character around in the game world and allows you to explore the world around you. It also allows you to engage in combat and find wondrous things. 

To move your character token, simply click it and drag it around the game grid.

### Using the map
#### Panning (moving)
When **left-clicking and dragging** with your mouse the map, you will be able to move the map around. 

### Zooming
To zoom, **use the scroll button** on your mouse, you can control the scale of the map and grid. Scrolling the mouse wheel up zooms the map in, scrolling the mouse wheel down zooms the map out.

### Notes
* Character tokens as of this release are not animated and do not display HP bars.