---
label: User Interface Features
order: 0
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

!!!info
In this release, there are some elements which you can see on the right-hand-side of the screen which you shouldn't be able to. For the time-being, the "Maps" and "Character" sections are mainly used by Game Masters. Speak to your Game Master and double-check which features they want to use and which ones they're OK with you using.
!!!

When you have logged in, Mythic Table provides you with a user interface so you can play your Virtual Tabletop adventures! This page will give a brief overview of the interface and how to use it to play your game. 

![The Mythic Table interface with all features annotated ](./../../images/players-user-interface-annotated.png)

Based on the numbers in the image below, the user interface has the following features displayed for you to use:

1. **Drawing Tool** - Allows the user to temporarily draw on the Grid.
2. **Fog Of War Tool** - A DM-only section for displaying Fog Of War (this will be hidden from players in future versions)
3. **Grid Options Tool** - Allows you to set various options for the grid such as snapping.
4. **Account Settings** - Allows you to view your account settings 
5. **Campaigns** - Allows you to view the current and other campaigns
6. **Return to Campaigns List** - Returns you to the main view where you can select campaigns
7. **Dice Roller Toolbar** - Contains dice you can click to roll.
8. **Chat Window** - A window where you can discuss your campaign and roll dice.
9. **Map Display Window** - A DM-only section for displaying all the maps within the campaign (this will be hidden from players in future versions)
10. **Character/Token Display Window** - A DM-only section for displaying all available tokens for the campaign (this will be hidden from players in future versions)
11. **Map/Grid Display** - The main display where you can view the board, and move your token(s).

