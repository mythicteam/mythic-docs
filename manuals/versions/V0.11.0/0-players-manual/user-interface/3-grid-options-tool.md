---
label: Grid options tool
order: 9
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

The grid options are tools you can use to make the map/grid display more usable for you. When moving your token around the map, it can be somewhat difficult at times to find where the token is, or to align your token on the map. 

### Hide grid mode
![The default map when the grid is turned off](./../../images/player-grid-hidden-disabled.png)

The **"Grid hidden"** option displayed allows the user to turn on and off the a grid that is displayed above the map so that the user can see the size of tiles on the map.

![The default map when the grid is turned on](./../../images/player-grid-hidden-enabled.png)
When the grid is turned on, you will see a grid of "plus" symbols arranged into a grid. They are circled on the image above.

### Snap to grid mode
Snap to grid aligns tokens placed on the grid/map in line with the grid displayed. when you drag and drop your token, it will be placed into the nearest grid square. When it is turned off, you can place the token wherever you like.

![When grid snap is turned on, tokens remain within a grid square](./../../images/player-grid-snap-enabled.png)

![When grid snap is turned off, the token can be placed in-between squares](./../../images/player-grid-snap-disabled.png)


### Grid finder mode
The grid finder mode is used for getting the grid/map display to align with an image provided. This mode is only available for Game Masters to use, as it allows them to align the grid of the grid/map display to a map image so that the tokens successfully scale to the size of the map.