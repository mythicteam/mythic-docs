---
label: Drawing tool
order: 11
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

![The drawing tool](./../../images/player-drawing-tool.png)

The drawing tool allows you to draw free-hand items on the grid so that you can draw attention to something for other people, or for the Game Master. 

All drawings that are made on the grid will disappear after a short period. This is intentional as it is intended to reduce clutter for both players and for the Game Master so that they do not have to keep clearing the grid of player drawings.

## Drawing something on the grid
![Drawing something on the grid/map](./../../images/player-drawing-tool-grid.png)

1. Select the "Drawing tool" from the left bar.
2. Select a "Color" from the list by clicking the circles displayed, or leave it as default.
3. Select a "Brush Size" from the list by clicking the circles displayed.
4. Move your mouse over the grid display (where the map is displayed) and click and hold the left mouse button to draw on the canvas.


### Color
Selecting one of the pre-defined colors will allow you to change the color of the drawn object. It is not possible to select colors other than the ones shown in the list.

### Brush Size
Selecting one of the circles from the list will increase the size of the 

### Notes
* If you wish to display a "dot", you will need to click and hold, and draw a small circle. 
* There is no cursor displayed when drawing on the grid/map.