---
label: Account settings tool
order: 8
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
![The account settings tool](./../../images/player-account-tools.png)

The account settings tool allows you to view and perform changes to your account. As of the current version of Mythic table, this only has basic functionality.

### Account
This will open your account in the what we call the Login Provider. Mythic table doesn't deal with logging in itself, instead it passes this responsibility off to the person running the Mythic Table Server (see the section "Logging in" for more information). This will open a link to view and edit your account within that login provider.

### Edit Profile
This section allows you to change your display name.

### Sign out
Signs you out completely from Mythic Table.