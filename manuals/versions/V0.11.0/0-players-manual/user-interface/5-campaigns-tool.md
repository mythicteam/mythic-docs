---
label: Campaigns menu
order: 7
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

The campaigns menu allows the user to create an invitation link for the campaign, or to switch to another campaign. Other options displayed will be hidden in future versions.

### Invitation link
Allows you to copy a link to the current campaign which you can then share with your friends, family, co-workers, lawyer, haberdasher, etc. 

### Open another campaign
No prizes for guessing what this does.