---
label: Chat window
order: 4
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

The chat window is used by players and Game Masters to communicate and also to perform dice rolls. 


![The chat window](./../../images/player-chat-display.png)

## Writing a chat message
As simple as it gets, type in a sassy, wondrous, beautiful, and eloquent message, and press enter and it will be sent to other players. Either that or type "LOL WUT" when you roll a 1 and face certain death. 

## Making a dice roll
To make a dice roll, simply type in the roll you want to make. The following types of rolls are supported:
* Regular dice roll (e.g. 3d6)
* Dice roll plus modifier (e.g. 1d10+5)
* Multiple combined dice rolls (e.g. 1d100+5+5d10+6)

## Note
* Rolls do not display things like attack types, skills, weapons, effects, advantage, and so on. This is planned for future releases.
* The text chat does not support commands like "/roll". This is planned for future releases.
* The text chat does not support writing text in the first person (e.g. "CharacterName hates Ogres"). This is planned for future releases.
* When new messages are received, the chat does not automatically scroll to the bottom.