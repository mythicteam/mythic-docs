---
label: Joining games
order: 1
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

As a player, you can either:

* Browse a list of games available on your local Mythic Table instance
* Join a game with a code given to you by the Game Master (GM)

### Browsing a list of games
![A list of games the player can view, showing campaigns available to the user](./../images/player-list-campaigns.png)
After you log in, you'll be presented with a list of games either you have created, or that have been shared with you. You'll also have access to the tutorial campaign which lets you run through various aspects of how Mythic Table works so you're familiar with how to play your part as a player. 

### Joining a game from a link provided to you
![Screen displayed after entering a link into your browser](./../images/player-join-campaign-with-link.png)
If your Game Master has already started a game, then you can ask them to share a link with you, which gives you a direct link to join their campaign. You'll be asked to sign in first if you haven't already, and then will be redirected to the game's page. It will show a brief description of the campaign and an image which your Game Master has chosen to visually represent it. 

* If you're sure it is the correct campaign, you can select **Join campaign**, and you'll join into the game.
* If you don't want to join the campaign, you can click **Decline**. 

Don't worry, if you click **Decline** by accident, you can get another invite from your Game Master!