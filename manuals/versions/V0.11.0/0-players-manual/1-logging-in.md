---
label: Logging in
order: 2
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---
When logging in you have the option of using either the Mythic Table website (http://www.mythictable.com/) or a third-party provider of Mythic Table.

!!!info Third-Party Providers
 The developers and organisation of Mythic Table do not have control of Third Party websites which run Mythic Table, so bear in mind that you may not receive the same level (or any level!) of support from them. We cannot support with login issues for sites other than the Mythic Table website.
!!!

## The MythicTable website
![The login screen for mythictable.com, run by the Mythic Table team](../images/player-login-mythic-instance.png)

Like every other thing on the internet, pretty much, you're going to need to log in. It's pretty much as you expect, so log in as you normally would any other application. If you want to register, then you can use whatever e-mail address you like to join and we'll remember your login information and campaigns you're running. However, we understand that some people might not want to do this, so we offer the option to continue as guest.

To create a new account, click "Sign Up", and to log in with existing details, use "Log In", as you are familiar with everywhere else on the interwebs.

### Third-party Mythic Table providers
![A common login screen for a service hosted by someone else](../images/player-third-party-login-screen.png)
If you're having problems with login, such as you have forgotten your password, you need to change account details, want your data, or want to delete your account, you're going to have to find the contact details for who runs that instance of Mythic Table and contact them. For most normal operations, Mythic Table supports log in, sign up, password reset features you're used to, so you should be OK!
