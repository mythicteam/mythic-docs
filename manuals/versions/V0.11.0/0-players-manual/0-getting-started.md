---
label: Getting started as a player
order: 3
authors:
  - name: Jordán Craw
    email: jordancraw@riseup[dawt]net
    link: https://gitlab.com/jordancraw
    avatar: https://gitlab.com/uploads/-/system/user/avatar/9872049/avatar.png
---

In this version of Mythic Table, you're limited somewhat in the options you have to join, but you'll get along just fine if you're wanting to get started on a game!

### Getting help if you're stuck
Documentation is great, but, we get you - sometimes it can be frustrating and there may be times you're not sure what's happening. That's cool! We're happy to help you, just come drop into our discord server and we can give you help.

* Join our Discord Server: https://discord.gg/c4bEWDQ

### Finding where to play
To start, you're going to need to get the URL of your Mythic Table Instance. By "Mythic Table Instance", we mean the URL you have to put into your browser to get to your game. You might be doing this via the Mythic Table website, but you also may be connecting to a website that provides Mythic Table for you to play on. 

Mythic is FOSS (Free and Open Source) software, so it means anyone can run it themselves, but also it is a non-profit organisation that runs an instance of Mythic Table for people to play on. You can also run Mythic Table yourself on your machine or LAN if you want. If you're not sure how to do that, check out the "Server Admin's Guide", which gives instructions on how to run Mythic Table locally or over the interwebs.
